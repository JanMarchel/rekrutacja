package com.JanMarchel.Rekrutacja.Controller;

import com.JanMarchel.Rekrutacja.DTO.EmployeeDTO;
import com.JanMarchel.Rekrutacja.Model.Employee;
import com.JanMarchel.Rekrutacja.Service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor

public class EmployeeController {

    EmployeeService employeeService;

    @GetMapping("/employees")
    public ResponseEntity<List<EmployeeDTO>> getAllEmployees() {
        try {
            List<EmployeeDTO> employees = employeeService.getAllEmployeesDTOs();
            return ResponseEntity.ok(employees);
        } catch (Exception exception)
        {
            exception.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/employees/{id}")
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable("id") long id) {
        try {
            Employee employee = employeeService.getEmployeeById(id);
            EmployeeDTO employeeDTO = employeeService.mapEmployeeToEmployeeDTO(employee);
            return ResponseEntity.ok(employeeDTO);
        } catch (Exception exception)
        {
            exception.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/employees")
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        try {
            employeeService.createEmployee(employeeDTO);
            return ResponseEntity.ok(employeeDTO);
        } catch (Exception exception)
        {
            exception.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
