package com.JanMarchel.Rekrutacja.Service;

import com.JanMarchel.Rekrutacja.Exception.EmployeeNotFoundException;
import com.JanMarchel.Rekrutacja.Model.Employee;
import com.JanMarchel.Rekrutacja.DTO.EmployeeDTO;
import com.JanMarchel.Rekrutacja.Repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor

public class EmployeeService {

    EmployeeRepository employeeRepository;

    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee createEmployee(EmployeeDTO employeeDTO) {

        Employee employee = Employee.builder()
                .firstName(employeeDTO.getFirstName())
                .lastName(employeeDTO.getLastName())
                .position(employeeDTO.getPosition())
                .build();

        Employee superior = employeeRepository.findByFirstNameAndLastName(employeeDTO.getSuperiorFirstName(),employeeDTO.getSuperiorLastName());

        if(superior != null) employee.setSuperior(superior);

        return saveUser(employee);
    }

    public Employee saveUser(Employee employee) {
        return employeeRepository.save(employee);
    }

    public EmployeeDTO mapEmployeeToEmployeeDTO(Employee employee) {
        if(employee.getSuperior() == null)
            return EmployeeDTO.builder()
                    .firstName(employee.getFirstName())
                    .lastName(employee.getLastName())
                    .position(employee.getPosition()).build();
        else {
            Employee superior = getEmployeeById(employee.getSuperior().getId());
            return EmployeeDTO.builder()
                    .firstName(employee.getFirstName())
                    .lastName(employee.getLastName())
                    .position(employee.getPosition())
                    .superiorFirstName(superior.getFirstName())
                    .superiorLastName(superior.getLastName())
                    .build();
        }
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }
    public List<EmployeeDTO> getAllEmployeesDTOs() {
        return findAllEmployees()
                .stream().map(this::mapEmployeeToEmployeeDTO)
                .collect(Collectors.toList());
    }
}
