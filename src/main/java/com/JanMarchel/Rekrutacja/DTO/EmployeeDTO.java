package com.JanMarchel.Rekrutacja.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmployeeDTO {
    private String firstName;
    private String lastName;
    private String position;
    private String superiorFirstName;
    private String superiorLastName;
}

