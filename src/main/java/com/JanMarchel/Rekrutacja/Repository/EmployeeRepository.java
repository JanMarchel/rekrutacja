package com.JanMarchel.Rekrutacja.Repository;

import com.JanMarchel.Rekrutacja.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    Employee findByFirstNameAndLastName(String firstName, String lastName);
}
